package controller;
import model.Usuario;
import model.Medico;
import model.Paciente;
import java.util.ArrayList;

public class ControladorMaisMedicos {
	
	ArrayList<Medico> listaMedicos = new ArrayList();
	
	ArrayList<Paciente> listaPacientes = new ArrayList();

    /*public void ControladorMaisMedicos() {
        listaMedicos = new ArrayList<Paciente>();
        listaPacientes = new ArrayList<Medico>();
    }*/
        
	public void adicionarMedico(Medico umMedico){
		listaMedicos.add(umMedico);
	}

	public void adicionarPaciente(Paciente umPaciente){
		listaPacientes.add(umPaciente);
	}
	
	public void removerMedico(Medico umMedico){
		listaMedicos.remove(umMedico);
	}
	
	
	public void removerPaciente(Paciente umPaciente){
		listaPacientes.remove(umPaciente);
	}
		
	public Medico buscarMedico(String nome){
		for(Medico umMedico: listaMedicos){
			if(umMedico.getNome().equalsIgnoreCase(nome)){
				return umMedico;
			}
		}
		return null;
	}

	
	public Paciente buscarPaciente(String nome){
		for(Paciente umPaciente: listaPacientes){
			if(umPaciente.getNome().equalsIgnoreCase(nome)){
				return umPaciente;
			}
		}
		return null;
	}
        
        public int medicosCadastrados(){
            return listaMedicos.size();
        }
        
        public int pacientesCadastrados(){
            return listaPacientes.size();
        }
	
}