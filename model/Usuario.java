package model;

public class Usuario {
	private String nome;
	private String email;
	
	
	// Método Construtores
	public Usuario() {
	}
	
	public Usuario(String nome) {
		this.nome = nome;
	}
	
	// Métodos GET e SET
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	

}