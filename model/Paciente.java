package model;

public class Paciente extends Usuario{
	private String id;
	private String estado;
	private Consulta consulta;
	
	// Métodos Construtores
	
	public Paciente(String nome, String id) {
		super(nome);
		this.id = id;
	}
	public Paciente() {
		super();
	}
	
	// Métodos GET e SET
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	
	
	
}